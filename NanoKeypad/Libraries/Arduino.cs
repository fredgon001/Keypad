﻿using System;
using System.IO.Ports;

namespace Program.Libraries
{
    public class Arduino
    {
        System.IO.Ports.SerialPort _serialPort;
        private bool IsClosed = false;

        public Arduino(SerialPort serialPort)
        {
            //Port = new System.IO.Ports.SerialPort();
            //Port.PortName = portName;
            //Port.BaudRate = portBaudRate;
            //Port.ReadTimeout = portReadTimeout;
            _serialPort = serialPort;
        }
        public void ClosePort() => IsClosed = true;
        public void LoopSerial()
        {
            
            while (!IsClosed)
            {
                try
                {
                    string arduinoInput = _serialPort.ReadLine();
                    if (arduinoInput != null || arduinoInput.Length > 0)
                    {
                        SendKeyboardInput(arduinoInput.ToCharArray()[0]);
                        Console.WriteLine(arduinoInput);
                    }
                }
                catch (Exception ex)
                {
                    //
                    if(ex.Message == "The port is closed.")
                    {
                       _serialPort.Close();
                        IsClosed = true;
                        throw new Exception("Keypad is disconected \n Select the right port and save.");
                    }
                }
            }
        }

        private void SendKeyboardInput(char keyPadChar)
        {
            ushort[] keypadTo_wScan = { 0x52, 0x4f, 0x50, 0x51, 0x4b, 0x4c, 0x4d, 0x47, 0x48, 0x49 };
            ushort keypadTo_wScanMultipricity = 0x37;
            ushort keypadTo_wScanHash = 0xE035;//divide
            //ushort keypadTo_wScanHash = 0xE0B5;//divide
            //ushort keypadTo_wScanHash = 0xE04A;//divide

            ushort keyToSend;

            switch (keyPadChar)
            {
                case '*':
                    keyToSend = keypadTo_wScanMultipricity;
                    break;
                case '#':
                    keyToSend = keypadTo_wScanHash;
                    break;
                default:
                    keyToSend = keypadTo_wScan[Convert.ToInt32(Char.GetNumericValue(keyPadChar))];
                    break;
            }

            InputSender.KeyboardInput[] inputs =
               {
                   new InputSender.KeyboardInput
                   {
                       wScan = keyToSend,
                       dwFlags = (uint)(InputSender.KeyEventF.KeyDown | InputSender.KeyEventF.Scancode),
                   },
                   new InputSender.KeyboardInput
                   {
                       wScan = keyToSend,
                       dwFlags = (uint)(InputSender.KeyEventF.KeyUp | InputSender.KeyEventF.Scancode)
                   }
            };
            //enviar la entrada
            InputSender.SendKeyboardInput(inputs);
        }
    }
}
