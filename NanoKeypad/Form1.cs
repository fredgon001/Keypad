﻿using System;
using System.Configuration;
using System.IO.Ports;
using System.Windows.Forms;
using System.Threading;
using Program.Libraries;

namespace NanoKeypad
{
    public partial class Form1 : Form
    {
        //private ConfigurationManager configurationManager;
        SerialPort SerialPort;
        Arduino Arduino;
        Thread _loopSerial;
        private readonly NotifyIcon _notifyIcon;
        public Form1()
        {
            InitializeComponent();
            _notifyIcon = new NotifyIcon();
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            SerialPort = new SerialPort();
            RefreshPorts();
            LoadConfiguration();
            StartArduinoPort();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            _notifyIcon.Icon = new System.Drawing.Icon("Resources/keypad.ico");
            _notifyIcon.Text = "NanoKeyboard";
            _notifyIcon.DoubleClick += NotifyIcon_Click;

            _notifyIcon.ContextMenuStrip = new ContextMenuStrip();
            _notifyIcon.ContextMenuStrip.Items.Add("Exit", null, OnExitClicked);
            _notifyIcon.Visible = true;
            //this.HideApplication();
        }

        private void NotifyIcon_Click(object sender, EventArgs e)
        {
            ShowApplication();
        }

        private void OnExitClicked(object sender, EventArgs e)
        {
            ExitApplication();
        }

        private void showNotifyIcon()
        {
            //base.SetVisibleCore(false);
            _notifyIcon.BalloonTipIcon = ToolTipIcon.Info;
            _notifyIcon.BalloonTipText = string.Format("NanoKeypad is running on the port {0}!!",SerialPort.PortName);
            _notifyIcon.BalloonTipTitle = "NanoKeypad initialized";
            _notifyIcon.ShowBalloonTip(2000);
        }

        private void RefreshPorts()
        {
            // Get a list of serial port names.
            string[] ports = SerialPort.GetPortNames();
            comboBoxPort.Items.Clear();
            comboBoxPort.Items.AddRange(ports);
        }

        private void StartArduinoPort()
        {
            SerialPort.PortName = GetConfigurationKey("Port");
            SerialPort.BaudRate = Convert.ToInt32(GetConfigurationKey("BaudRate"));
            SerialPort.ReadTimeout = Convert.ToInt32(GetConfigurationKey("ReadTimeout")); 
            Arduino = new Arduino(SerialPort);
            _loopSerial = new Thread(Arduino.LoopSerial);
            try
            {
                SerialPort.Open();
                _loopSerial.Start();
                showNotifyIcon();
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Arduino was not found {0}. /n Error: {1}", SerialPort.PortName, ex.Message));

            }
        }
        private string GetConfigurationKey(string key)
        {
            return ConfigurationManager.AppSettings.Get(key);
        }

        private void LoadConfiguration() 
        {
            comboBoxPort.SelectedIndex = comboBoxPort.Items.IndexOf(ConfigurationManager.AppSettings.Get("Port"));
            txtBaudRate.Text = ConfigurationManager.AppSettings.Get("BaudRate");
            txtReadTimeOut.Text = ConfigurationManager.AppSettings.Get("ReadTimeout");
        }
        private void SaveConfiguration()
        {
            try
            {
                var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var settings = configFile.AppSettings.Settings;
                settings["BaudRate"].Value = txtBaudRate.Text;
                settings["ReadTimeout"].Value = txtReadTimeOut.Text;
                settings["Port"].Value = comboBoxPort.SelectedItem.ToString();

                configFile.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);
                MessageBox.Show("Data was saved!");
                Console.WriteLine("Close arduino serial port");
                if(SerialPort.IsOpen) SerialPort.Close();
                Console.WriteLine("Restaring arduino serial port");
                StartArduinoPort();
                //minimize form
            }
            catch (Exception ex)
            {
                MessageBox.Show("Something happens! error: "+ex.Message);
                throw;
            }
            
        }


        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveConfiguration();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            RefreshPorts();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            HideApplication();
        }
        private void ExitApplication()
        {
            if (_loopSerial.IsAlive) _loopSerial.Interrupt();
            Arduino.ClosePort();
            //if (SerialPort.IsOpen) SerialPort.Close();
            _notifyIcon.Dispose();
            Application.Exit();
        }
        
        private void ShowApplication()
        {
            this.WindowState = FormWindowState.Normal;
            this.Show();
            //_notifyIcon.Visible = false;
            this.Activate();
        }
        private void HideApplication()
        {
            //this.WindowState = FormWindowState.Minimized;
            this.Hide();
            _notifyIcon.Visible = true;
        }

        private void btnExit_MouseClick(object sender, MouseEventArgs e)
        {
            HideApplication();
        }

        //private void _notifyIcon_MouseClick(object sender, MouseEventArgs e)
        //{
        //    ShowApplication();
        //}
    }
}
